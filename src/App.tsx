import { Route, Routes } from "react-router-dom";

import NotFound from "./container/404";
import Home from "./container/Home/index";


function App() {
  return (
    <div id='App'>

      <Routes>
          <Route path={'*'} element={<NotFound />} />
          <Route path={'/'} element={<Home />} />
      </Routes>

    </div>
  );
};

export default App;